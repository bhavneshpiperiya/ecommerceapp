package in.bhavneshit.bhavnesh.service;

import in.bhavneshit.bhavnesh.entity.Product;
import java.lang.Long;
import java.util.List;

/**
 * @author:BHAVNESH PIPERIYA  
 *  Generated F/w:SHWR-Framework 
 */
public interface IProductService {
	Long saveProduct(Product product);

	void updateProduct(Product product);

	void deleteProduct(Long id);

	Product getOneProduct(Long id);

	List<Product> getAllProducts();
}
