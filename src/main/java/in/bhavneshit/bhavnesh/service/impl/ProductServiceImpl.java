package in.bhavneshit.bhavnesh.service.impl;

import in.bhavneshit.bhavnesh.entity.Product;
import in.bhavneshit.bhavnesh.repo.ProductRepository;
import in.bhavneshit.bhavnesh.service.IProductService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:BHAVNESH PIPERIYA  
 *  Generated F/w:SHWR-Framework 
 */
@Service
public class ProductServiceImpl implements IProductService {
  @Autowired
  private ProductRepository repo;

  @Override
  @Transactional
  public Long saveProduct(Product product) {
    return repo.save(product).getId();
  }

  @Override
  @Transactional
  public void updateProduct(Product product) {
    repo.save(product);
  }

  @Override
  @Transactional
  public void deleteProduct(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public Product getOneProduct(Long id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<Product> getAllProducts() {
    return repo.findAll();
  }
}
