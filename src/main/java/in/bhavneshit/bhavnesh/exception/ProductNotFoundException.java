package in.bhavneshit.bhavnesh.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class ProductNotFoundException extends RuntimeException {
  public ProductNotFoundException() {
  }

  public ProductNotFoundException(String message) {
    super(message);
  }
}
