package in.bhavneshit.bhavnesh.repo;

import in.bhavneshit.bhavnesh.entity.Product;
import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author:BHAVNESH PIPERIYA  
 *  Generated F/w:SHWR-Framework 
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
